<?php
    require_once 'core/functions.php';

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    $pathBackground = pathJoin("captcha_background.jpg");
    if(!file_exists($pathBackground)) {
        echo "Файл с фоновой картинкой не найден";
        exit;
    }

    $pathFont = pathJoin("Worstveld Sling Bold.ttf");
    if(!file_exists($pathFont)) {
        echo "Файл шрифта не найден";
        exit;
    }

    $width = 150;
    $height = 60;

    list($widthBackgound, $heightBackgound) = getimagesize($pathBackground);

    $x = rand(0, $widthBackgound - $width);
    $y = rand(0, $heightBackgound - $height);

    $code = genCode();

    $imageBackground = imagecreatefromjpeg($pathBackground);
    $image = imagecreatetruecolor($width, $height);

    $color = imagecolorallocate($image, 0, 0, 0);

    imagecopy($image, $imageBackground, 0, 0, $x, $y, $width, $height);
    $x = rand(5, 15) + (7 - strlen($code)) * 12;
    for ($i = 0; $i < strlen($code); $i++) {
        $char = substr($code, $i, 1);
        $size = rand(20, 36);
        $angle = -15 + rand(0, 30);
        imagettftext($image, $size, $angle, $x, rand(35, 45), $color, $pathFont, $char);
        $x += $size * rand(55, 65) / 100;
    }

    $_SESSION['code'] = strtolower($code);

    header('Content-Type: image/jpg');

    imagejpeg($image);

    imagedestroy($image);
