<?php
    require_once 'core/functions.php';

    $pathCertificate = pathJoin("certificate.png");
    if(!file_exists($pathCertificate)) {
        echo "Файл с шаблоном сертификата не найден";
        exit;
    }

    $pathFont = pathJoin("Marianna.ttf");
    if(!file_exists($pathFont)) {
        echo "Файл шрифта не найден";
        exit;
    }

    $name = $_SESSION["user"]['login'];
    $result = $_SESSION["result"];
    $score = $_SESSION["score"];

    //var_dump($_SESSION);

    $image = imagecreatefrompng($pathCertificate);
    $color = imagecolorallocate($image, 0, 0, 0);

    imagettftext($image, 32, 2, 240, 300, $color, $pathFont, "{$name}, выполнил тест");
    imagettftext($image, 32, 2, 140, 360, $color, $pathFont, "с результатом: {$score}. Верных ответов: {$result}%");
    imagettftext($image, 14, 0, 555, 416, $color, $pathFont, date('Y.m.d H:i:s'));

    header('Content-Type: image/png');

    imagepng($image);

    imagedestroy($image);

?>
