<?php
    require_once 'core/functions.php';

    define("NUMBER_SIMPLE_ATTEMPTS", 5);
    define("NUMBER_DIFFICULT_ATTEMPTS", 6);

    if (isAutorised()) {
        redirect('list');
    }

    $errors = [];
    $guessing = !empty(getParamSession('guessing'));
    $timeAccessRestrictions = getParamSession('timeAccessRestrictions');
    if (isset($timeAccessRestrictions) && $timeAccessRestrictions < time()) {
        $timeAccessRestrictions = null;
        $_SESSION['timeAccessRestrictions'] = null;
    }
    $probablyBot = isset($timeAccessRestrictions);

    // информации о текущей сессии сохраним в куки, по ней будем опредялять пользователя
    // правда, очистив куки пользователь уберет блокировку
    setcookie('PHPSESSID', $_COOKIE['PHPSESSID'], time() + 24*60*60);

    if (isPost()) {
        if ($guessing && strtolower(getParamPost('captcha')) != getParamSession('code')) {
            if (empty(getParamPost('captcha'))) {
                $errors[] = 'Необходимо указать текст на картинке';
            } else {
                $errors[] = 'Введенный текст отличается от того, что на картинке';
            }
        } else {
            $counter = counterAttempt('numberAuth');

            $guessing = $counter >= NUMBER_SIMPLE_ATTEMPTS;
            $_SESSION['guessing'] = $guessing;
            if (login(getParamPost('login'), getParamPost('password'))
                && (!$guessing || strtolower(getParamPost('captcha')) == getParamSession('code'))
            ) {
                redirect('list');
            } else {
                $errors[] = 'Ошибка авторизации';
                $probablyBot = $counter >= (NUMBER_SIMPLE_ATTEMPTS + NUMBER_DIFFICULT_ATTEMPTS);
            }
        }
    }

    if ($probablyBot) {
        $_SESSION['timeAccessRestrictions'] = time() + 60*60;
        echo 'Подождите часок, мы вас заблокировали, а затем попробуйте снова';
        exit;
    }
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Авторизация</title>
</head>
<body>
<form method="post">
    <h1>Авторизация</h1>
    <ul>
        <?php foreach ($errors as $error) {
            echo '<li>' . $error . '</li>';
        } ?>
    </ul>
    <br>
    <label>
        Логин:
        <input type="text" name="login" placeholder="Логин">
    </label><br>
    <label>
        Пароль:
        <input type="password" name="password" placeholder="Пароль">
    </label>
    <br>
    <br>
    <?php
        if ($guessing) {
            echo '<label>
                <img src="captcha.php" alt="Капча">
            </label><br>
            <label>
            Код с картинки:
                <input type="text" name="captcha" autocomplete="off">
            </label><br>';
        }
    ?>
    <input type="submit" value="Войти">
</form>
</body>
</html>
