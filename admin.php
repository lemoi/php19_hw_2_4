<?php
    require_once 'core/functions.php';

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    if (!isAutorised()) {
        redirect('index');
    }

    if (!isAdmin()) {
        http_response_code(403);
        echo "Добавление тестов доступно только для зарегистрированных пользователей";
        exit;
    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.4</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <!-- <a href="test.php">Пройти тест</a> -->
        <hr>
    </div>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="userfile">
        <input type="submit" value="Отправить">
    </form>
</body>
</html>

<?php
    if (isset($_FILES) and !empty(getParamFiles('userfile'))) {
        $file = $_FILES["userfile"];
        $result = testIsValid($file["tmp_name"]);
        if ($result !== true) {
            echo $result;
            return;
        }

        $path = pathJoin(['tests', $file['name']]);
        if (move_uploaded_file($file['tmp_name'], $path)) {
            echo "Файл {$file['name']} передан <br>";
        } else {
            echo "Файл не передан";
        }
    }
?>