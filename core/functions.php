<?php
    define('USER_ADMIN', 'admin');
    define('USER_PASS', 'pass');

    session_start();

    function isPost() {
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }

    function getParamPost($name) {
        return array_key_exists($name, $_POST) ? $_POST[$name] : null;
    }

    function getParamGet($name) {
        return array_key_exists($name, $_GET) ? $_GET[$name] : null;
    }

    function getParamFiles($name) {
        return array_key_exists($name, $_FILES) ? $_FILES[$name] : null;
    }

    function getParamSession($name) {
        return array_key_exists($name, $_SESSION) ? $_SESSION[$name] : null;
    }

    function counterAttempt($name) {
        $counter = getParamSession($name);
        $counter = isset($counter) ? $counter + 1 : 1;
        $_SESSION[$name] = $counter;
        return $counter;
    }

    function login($login, $password) {
        if (empty($login)) {
            return false;
        }

        $guest = empty($password);
        $user = getUser($login);
        if (empty($user) && !$guest) {
            // используем введенные данные для регистрации
            createUser($login, $password);
        } elseif ($user['password'] != $password) {
            return false;
        }

        $_SESSION['user'] = ['login' => $login, 'guest' => $guest];
        return true;
    }

    function getAuthorisedUser() {
        return empty($_SESSION['user']) ? [] : $_SESSION['user'];
    }

    function isAutorised() {
        return !empty($_SESSION['user']);
    }

    function isAdmin() {
        return isAutorised() && $_SESSION['user']['guest'] === false;
    }

    function logout() {
        session_destroy();
    }

    function redirect($page) {
        header('Location: ' . $page . '.php');
        exit;
    }

    function getUser($login) {
        $filename = implode(DIRECTORY_SEPARATOR, [__DIR__, "users", $login . ".json"]);
        if (file_exists($filename)) {
            $data = file_get_contents($filename);
            $user = json_decode($data, true);
        } else {
            $user = null;
        }
        var_dump($user);
        var_dump($filename);
        return $user;
    }

    function createUser($login, $password) {
        $filename = pathJoin(["users", $login . ".json"]);
        $user = ['login' => $login, 'password' => trim($password)];
        file_put_contents($filename, json_encode($user));
    }

    function pathJoin($paths) {
        if(is_array($paths)) {
            $arrPaths = $paths;
            array_unshift($arrPaths, __DIR__, '..');
        } else {
            $arrPaths = [__DIR__, '..', (string) $paths];
        }
        return implode(DIRECTORY_SEPARATOR, $arrPaths);
    }

    function testIsValid($path) {
        // Возвращаем true в случае корректного теста, в противном случае описание ошибки
        $content = file_get_contents($path);
        if(!$content) {
            return "Не удалось получить содержимое файла";
        }
        $tests = json_decode($content, true);
        if(!is_array($tests)) {
            return "Файл с тестом должен быть json содержащим массив вопросов";
        }
        $importantFields = ["question", "answer", "correct"];
        $i = 1;
        $prefix = "Вопрос {$i}. ";
        foreach ($tests as $test) {
            foreach ($importantFields as $field) {
                if(!array_key_exists($field, $test)) {
                    return $prefix . "Отсутствует обязательное поле {$field}";
                }
            }
            if(!(is_string($test["question"])&&(is_string($test["correct"])||is_int($test["correct"])))) {
                return $prefix . "Поле question - должно содержать описание вопроса, а correct - ключ корректного ответа в массиве answer";
            }
            if(!(is_array($test["answer"])&&(array_key_exists($test["correct"], $test["answer"])))) {
                return $prefix . "В списке ответов на вопрос отсутствует ключ верного ответа указанный в поле correct";
            }
            $i++;
        }
        return true;
    }

    function genCode($maxLen = 7) {
        $chars = 'abdefhknrstyz23456789';
        $max = strlen($chars) - 1;
        $str = '';
        for ($i = 0; $i < rand(4, $maxLen); $i++) {
            $char = substr($chars, rand(0, $max), 1);
            $str .= rand(0, 2) == 0 ? $char : strtoupper($char);
        }
        return $str;
    }

?>