<?php
    require_once 'core/functions.php';

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    if (!isAutorised()) {
        redirect('index');
    }

    $path = pathJoin(['tests', '']);
    $nameTest = getParamGet('name');
    if(empty($nameTest)) {
        $file = glob($path . '*.json')[0];
    } else {
        $file = $path . $nameTest . '.json';
    }

    if(!(file_exists($file)&&is_file($file))) {
        http_response_code(404);
        echo '<h1>Ошибка 404</h1>Нет такой страницы';
        exit;
    }

    $nameTest = basename($file, ".json");
    $json = file_get_contents($file);
    $test = json_decode($json, true);

    $_SESSION['test'] = $nameTest;

    if(isPost()) {

        $correctAnswer = 0;
        $i = 1;
        foreach ($test as $question) {
            $answerName = 'answer' . $i;
            if (getParamPost($answerName) === $question["correct"]) {
                $correctAnswer++;
            }
            $i++;
        }

        $result = round($correctAnswer / count($test) * 100);
        if($result >= 90) {
            $score = 5;
        } elseif ($result >= 80) {
            $score = 4;
        } elseif ($result >= 60) {
            $score = 3;
        } else {
            $score = 2;
        }

        $_SESSION["score"] = $score;
        $_SESSION["result"] = $result;
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.4</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <?php if(isAdmin()) {
            echo '<a href="admin.php">Добавить тест</a>';
        } ?>
        <a href="list.php">Выбрать тест</a>
        <!-- <a href="test.php">Пройти тест</a> -->
        <?php if(isAdmin()) {
            echo '<a href="delete.php">Удалить тест</a>';
        } ?>
        <hr>
    </div>
    <form method="post">
        <h1>Тест <?php echo $nameTest; ?></h1>
        <?php
            $tmpQuestion = '<h2>%3 Вопрос %1. %2</h2>';
            $tmpAnswer = '<input type="radio" name="%1" value="%2"><span style="%4">%3</span><br>';

            $i = 1;
            foreach ($test as $question) {
                $answerName = "answer" . $i;
                $success = isPost() ? (getParamPost($answerName) === $question["correct"] ? '&#10004;' : '&#10008;') : '';
                $tag = str_replace(["%1", "%2", "%3"], [$i, htmlspecialchars($question["question"]), $success], $tmpQuestion);
                echo $tag;

                foreach ($question["answer"] as $key => $answer) {
                    $styleAnswer = isPost() && $key === $question["correct"] ? "color: green;": "color: black;";
                    $styleAnswer .= (getParamPost($answerName) === $key ? "background-color: yellow;": "");
                    echo str_replace(
                            ["%1", "%2", "%3", "%4"],
                            [$answerName, htmlspecialchars($key), htmlspecialchars($answer), $styleAnswer],
                            $tmpAnswer
                    );
                }
                $i++;
            }
        ?>
        <hr>
        <input type="submit" value="Отправить">
    </form>

    <?php
        if(isPost()) {
            echo '<h2>Ваш сертификат:</h2>';
            echo '<img src="certificate.php">';
        }
    ?>

</body>
</html>

