<?php
    require_once 'core/functions.php';

    if (!isAdmin()) {
        http_response_code(403);
        echo "Удаление тестов доступно только для авторизованных пользователей";
        exit;
    }

    $nameTest = $_SESSION['test'];
    if(!isset($nameTest)||empty($nameTest)) {
        echo "Не указан тест для удаления";
        exit;
    }

    $path = pathJoin(["tests", $nameTest . '.json']);
    if(!(file_exists($path)&&is_file($path))) {
        echo "Выбранный тест не существует";
        exit;
    }

    unlink($path);
    redirect('list');
?>
